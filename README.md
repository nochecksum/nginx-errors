![](preview.png)

[![stability-stable](https://img.shields.io/badge/stability-stable-green.svg)](#)  [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)  [![cool-factor](https://img.shields.io/badge/cool%20factor-outrageous-%23ff6c00.svg)](#)

This is a pack of 44 custom HTML error messages for Nginx servers used on some high traffic websites. Issues and pull requests welcomed!

* Includes almost all 4xx and 5xx errors (except for HTTP status codes where an error message would be inappropriate)

* Also includes nginx-specific error messages, and 418 and 420 for your own applications

* Small output with no SSI, variables or rewrites required (ideal for high-traffic servers)

* One external CSS and one external image file are used in error messages. These work fine on sites with strict CSRF policies which block inline scripts and styles

* The CSS and image asset filenames are obfuscated through nginx location directives to reduce the chance of naming conflicts on hosted sites

* Good browser support


# Installation

First install it on your server:

```bash
git clone git@gitlab.com:nochecksum/nginx-errors.git
cd nginx-errors
sudo mkdir /usr/share/nginx/errors
sudo cp html/* /usr/share/nginx/errors/
sudo chown root: /usr/share/nginx/errors/*
sudo chmod 644 /usr/share/nginx/errors/*
sudo cp errors.conf /etc/nginx/snippets/
sudo chown root: /etc/nginx/snippets/errors.conf
sudo chmod 640 /etc/nginx/snippets/errors.conf
```

Now include it in your server blocks:

```
# /etc/nginx/sites-available/001-test.com

server {
	# ... your listen, server_name, ssl, etc

	# Custom error pages
	include snippets/errors.conf;

	# ... your location {} etc
}
```

To enable custom error messages for PHP/etc responses, include the line `fastcgi_intercept_errors on;` in your nginx PHP-FPM directives. Now you can `http_response_code(418)` from your scripted tea pot.


# Adding new error codes

Create the error page's HTML in `/usr/share/nginx/errors`. Then edit `/etc/nginx/snippets/errors.conf` and include a new `error_page` directive. Check your syntax by running `nginx -t` and, if that goes well, restart nginx. Your new error message should be available in all configured sites on that box.


# Avoid conflicts

Don't use URIs in your application starting with `/nginx/`.


# Credits

The icon is part of the [Ionicons pack](https://ionicons.com/) and coloured with Inkscape or Gimp or both.

Descriptions of each error message are based on descriptions on the [Wikipedia list of HTTP status codes](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes).

Thanks to [@HBruijn](https://serverfault.com/users/37681/hbruijn) for helping to leanify my overly verbose nginx config snippet.


# Give something back

If you deploy this project on a busy production server, please consider buying yourself a coffee :heart:
